const ROOT = 42;

function addDepthAndSize(reportMap, curr, depth = 0) {
  reportMap[curr].depth = depth;
  depth += 1;
  let weight = 1;
  let isVisible = reportMap[curr].isVisible;
  for (let person of reportMap[curr].reports) {
    const res = addDepthAndSize(reportMap, person.slug, depth);
    weight += res.weight;
    isVisible = isVisible || res.isVisible;
  }
  reportMap[curr].reports.sort((a, b) => a.weight - b.weight);
  reportMap[curr].weight = weight;
  reportMap[curr].isVisible = isVisible;
  return { weight, isVisible };
}

export const getters = {
  hierarchy: (state, getters, rootState) => {
    const reportMap = rootState.team.reduce((all, person) => {
      let reportsTo = person.reports_to || ROOT;
      const slug = person.slug;
      const isVisible = !rootState.hidden.includes(slug);

      if (!all[reportsTo]) {
        all[reportsTo] = { reports: [], isVisible: false };
      }

      all[reportsTo].reports.push(slug);

      const reports = (all[slug] && all[slug].reports) || [];

      all[slug] = { ...person, isVisible, matchesSearch: isVisible, reports };

      return all;
    }, {});

    for (var key in reportMap) {
      reportMap[key].reports = reportMap[key].reports.map(x => reportMap[x]);
    }

    addDepthAndSize(reportMap, ROOT);

    return reportMap[42].reports;
  }
};
