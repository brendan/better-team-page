import Vue from "vue";
import VueLazyLoad from "vue-lazyload";

Vue.use(VueLazyLoad, {
  loading: "https://about.gitlab.com/images/gitlab-logo-extra-whitespace.png",
  error: "https://about.gitlab.com/images/gitlab-logo-extra-whitespace.png",
  attempt: 1,
  observer: true
});
