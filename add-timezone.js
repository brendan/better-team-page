const fs = require("fs");
const yaml = require("yaml-js");

const mapJSON = require("./team.json");
const { flag } = require("country-emoji");
const geoTz = require("geo-tz");
const axios = require("axios");
const iso = require("iso-3166-1");

if (!process.env.GEONAMES_USERNAME) {
  throw new Error("env variable GEONAMES_USERNAME must be set");
}

let CD = null;

const countryInfo = async () => {
  if (CD) {
    return CD;
  }
  const response = await axios({
    method: "get",
    url: `https://secure.geonames.org/countryInfo`,
    params: {
      type: "json",
      username: process.env.GEONAMES_USERNAME,
    },
  });

  const status = response?.data?.status || false;
  if (status) {
    throw new Error(
      `GeoNames raised error ${status.value}: ${status.message}. Search: ${response.request.path}`
    );
  }

  CD = response?.data?.geonames || [];
  return CD;
};

geoTz.preCache(); // optionally load all features into memory

const teamYML = yaml.load(fs.readFileSync("./team.yml", "utf8"));

const matchMember = ({ slug }) => {
  return mapJSON.team.find((x) => x.slug === slug);
};

function getMemberPicture(picture) {
  if (picture.startsWith("https://")) {
    return picture;
  }
  const pictureName = picture.replace(/\.(png|jpe?g)$/gi, "");
  return `https://about.gitlab.com/images/team/${pictureName}-crop.jpg`;
}

function buildChain(reports_to, array) {
  if (!reports_to) {
    return [];
  }
  const manager = array.find((x) => x.slug === reports_to);
  return [...buildChain(manager.reports_to, array), reports_to];
}

async function getCountryInfoForCountry(countryCode) {
  if (!countryCode) {
    return null;
  }
  const info = (await countryInfo()).find((x) => x.countryCode === countryCode);

  if (!info) {
    throw new Error(`Could not find population for ${countryCode}`);
  }
  return { ...info, population: parseInt(info.population, 10) };
}

function getCountryCode(country) {
  if (!country || ["Remote"].includes(country)) {
    return null;
  }
  const res = iso.whereCountry(country);

  if (!res) {
    console.log(country);
  }

  return res.alpha2;
}

async function getCountryPopulation(team) {
  const acc = {};

  for (const member of team) {
    if (!member.countryCode) {
      continue;
    }

    if (!acc[member.countryCode]) {
      acc[member.countryCode] = await getCountryInfoForCountry(
        member.countryCode
      );
    }
  }

  return acc;
}

async function main() {
  const team = teamYML
    .map((teamMember) => {
      const matchedMember = matchMember(teamMember);

      const picture = getMemberPicture(teamMember.picture);

      if (matchedMember) {
        let timezone;
        if (matchedMember.location) {
          timezone = geoTz(...matchedMember.location)[0];
        }

        return {
          ...teamMember,
          ...matchedMember,
          timezone,
          picture,
          countryEmoji: flag(matchedMember.countryCode),
        };
      }

      const countryCode = getCountryCode(teamMember.country);

      return {
        ...teamMember,
        picture,
        countryCode,
        countryEmoji: flag(teamMember.country),
      };
    })
    .map((member, index, array) => {
      const reportChain = buildChain(member.reports_to, array);

      return { ...member, reportChain };
    });

  const countryData = await getCountryPopulation(team);

  fs.writeFileSync(
    "./static/team.json",
    JSON.stringify(
      {
        version: `${mapJSON.version}-with-full-data`,
        team,
        countryData,
      },
      null,
      2
    )
  );
}

main()
  .then(() => {
    console.log("SUCCESS");
  })
  .catch((e) => {
    console.error("An error happened");
    console.error(e);
    process.exit(1);
  });
