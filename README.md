# better-team-page

> Better team page with availability information

## Prerequisites

- You will need a [GeoNames](https://www.geonames.org/login) account.
- Once you have an account, set the environmental variable `GEONAMES_USERNAME` to your account's username.

## Build Setup

```bash
# install dependencies
$ npm install # Or yarn install
$ ./build.sh # Download required team files

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
